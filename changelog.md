# Changelog

## 6.2.6
- fix fail of analysis if no sample groups were specified

## 6.2.5
- fix parsing of Proteome discoverer output that lacks a column named "Master" by calculating the mean value of the non-zero protein abundancies for all protein groups

## 6.2.4
- fix dbcan tasks: erroneous parsing of hmmer output resulted in many missed annotations. Previously the wrong columns (1/3) were parsed from hmmer results, which resulted in a lot of missed hits. The correct columns are 0 and 2 for hmmscan and hmmsearch respectively.
- internal: refactor building and writing of summary for comprehensibility.
- internal: remove second accession column from id2annot map files
- internal: rename column names of dbcan, pfam, tigrfam maps

## 6.2.3
- fix tigrfam download links
- fix memory issues during lca calculation. Large numbers of protein groups in combination with many samples lead to large RAM consumption. Protein Group and Sample information is now stored in a sqlite db (`pgs/protein_groups_db.sql`), reducing the memory requirements to a minimum.
- fix order of summary columns: When using sample groups, the name and title of summary columns did not match (affected: quant and mafft columns). Krona plots were not affected.
- fix Proteome Discoverer Parsing for groups without master protein (#84). If a group has no master protein, it's abundance is set to the mean of all member protein abundance values
- fix emapper v4 analysis (set block size parameter only for emapper v5)

## 6.2.2
- fix #82: prophane.de: eggnog jobs fail with memory error for some query fasta files
  - fixed by setting the block_size parameter based on the size of the all.faa fasta file: if the fasta file is larger than 10MB: block_size = 10 / size_in_MB(all.faa), rounded to one decimal. For smaller fasta files, the block_size is set to 2.

## 6.2.1
- fix eggnog v5 download
- fix eggnog v5 result mapping
- fix emapper log redirection, now is properly written to {task_file_name}.log

## 6.2
- add support for eggnog database version 5.0.2

## 6.1.1
- fix issue 80: only download ncbi_taxdump database for taxonomic analyses
- fix mafft workflow if no protein groups with more than one accession are present
- fix prophane crash upon executing `prophane list-dbs` on outdated databases. DBs are now automatically migrated.
- add `prophane --version` parameter to cli
- doc: adapt installation instructions to include direct conda installation, remove setup.sh

## 6.1
- add support for gzipped fasta files as input
- fix crash during parsing of Proteome Discoverer Output if it contains protein groups without any quantification values for the master protein. Now, Prophane ignores these protein groups.

## 6.0.5
- add support for large mzIdentML files

## 6.0.4
- refactor code to parse search result using snakemake (previously: plain python)

## 6.0.3
- fix command line interface option "prepare-dbs". It now accepts additional snakemake options and will work if the job 
  config contains acc2annot_mapper tasks

## 6.0.2
- fix database migration for setups that contain multiple versions of the same db 

## 6.0.1
- fix prophane path detection in CLI

## 6.0
### Breaking Changes
change of command line interface:

    prophane -> prophane.py run
    prophane --list-dbs -> prophane.py list-dbs
    prophane --list-styles -> prophane.py list-styles

### Features
Automatically download databases that are specified in the job-config. On first run of prophane, run prophane init {DB_DIR} where DB_DIR is an empty or non existant directory. To execute prophane, run prophane run {path-to-job-config}.

### Changes
* bump the db_schema version to 5
* task and plot files now include the database version number instead of the md5sum: tasks/{annot_type}_annot_by_{tool}_on_{db_type}.v{db_version}.task{taskid}.{ext}

## 5.1.1
- add mamba dependency

## 5.1
- add parser for proteome discoverer output

## 5.0.2
- fix mztab parser skipping samples without associated spectra and not counting all spectra

## 5.0.1
- fix parsing of spectra IDs from protein group yaml and some test files

## 5.0.0
### Breaking Changes
LCA determination of proteins with multiple annotations: If a species/lineage is found for all protein accessions in a protein group, this species/lineage is chosen as LCA. Before: if any other species was determined --> various.

### Features
LCA determination with two different methods:
1. (default) per protein group (with threshold, default: 1). If the ratio of proteins assigned to an ancestor is at least as high as the threshold, this ancestor is assigned to the entire group (multiple above threshold --> lca with highest support). Changes previous default behaviour. For example if a species/lineage is found for all protein accessions in one group, this species/lineage has a support of 100% and is chosen by the new lca-methode as LCA. Before: if any other species was determined --> various.
2. democratic: takes the one with highest occurrence among all protein groups in the whole task
    
New columns in summary.txt: lca-support, describing the number of proteins/spectra assigned to the respective LCA. Also the summary rule is adjusted for csv export, we only have to adjust the separator and includes spectra in output (if available).

## 4.3.0          
- add additional locations for general config: 
    - `{PROPHANE_INSTALL_DIR}/general_config.yaml`
    - `$HOME/.config/prophane.yaml`
    - `/etc/prophane.yaml`
- add additional input formats mzidentml and mztab (see documentation of input config)

## 4.2.2
- fix crash due to failing of mafft alignment for very large numbers of protein groups
- fix scaffold results parsing if one of the sample columns contains non-string values.

## 4.2.1
- fix version string

## 4.2
- adapt scaffold parser to new scaffold output format (maintaining compatibility with old format)
- search result parsing: remove decoys before checking accession format
- test.sh: move working directory to `${TMPDIR:-.}/prophane_test-${USER:-}`
- test.sh: export test environment to `${TMPDIR:-.}/prophane_test-${USER:-}/test_environment.yaml`

## 4.1
- prophane can now parse non-UTF8-encoded search result and fasta files

## 4.0.6
- fix `--list-styles` command line option. A styles in the styles subfolder are now listed if they can be parsed.

## 4.0.5
- add support for MPA-server multisample format (produced by MPA >=3.4).The new MPA multisample format allows automatic parsing of sample descriptors. This way, users no longer need to explicitly list the sample descriptors for parsing by Prophane.
- fix deprecation warnings for string escape sequences and pandas

## 4.0.4
- improved error message for illegal accessions in proteomic search result: print illegal accessions instead of lists 
containing illegal accessions among legal ones

## 4.0.3
- speed up resolving of base environment (environment.yaml) by reordering conda channels
- add test for resolving of environment.yaml to CI
- bump snakemake dependency to >=5.7.0
- speed up tests by reusing rule specific conda environments

## 4.0.2
- reduce IO overhead by executing mafft related rules (run_mafft, report_mafft) once for all protein groups 
instead of once per protein group
- fix: minimal/maximal sequence length calculation now correctly handles gaps in the alignment

## 4.0.1
- include protein group number in mafft related progress messages

## 4.0.0
- BREAKING: rename main execution script from run.sh to prophane
- BREAKING: remove automatic activation of conda environment from main execution script to allow packaging for distribution with conda.
The conda environment for prophane now must be activated manually prior to prophane execution.

## 3.3.1
- fix: fail on non-defined task-parameters in config yaml

## 3.3.0
- Added new config key `search_result` in input section, which can be used in combination with all search result styles,
    making style-specific file keys obsolete
- Make `sample_groups` config section optional for input styles that allow it to reduce the number of mandatory 
    execution arguments  
  
  styles for which `sample_groups` is optional:
  - generic.yaml
  - mpa_1_8_x.yaml
  - scaffold_4_8_x.yaml
  
  styles for which `sample_groups` is mandatory:
  - mpa_server_multisample.yaml

## 3.2.0
- Added support for [FOAM DB](https://cbb.pnnl.gov/portal/software/FOAM.html)
- Added support for [Resfams DB](http://www.dantaslab.org/resfams)
- Added support for [CAZy/canDB](http://bcb.unl.edu/dbCAN2/download/Databases/)

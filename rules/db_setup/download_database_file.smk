from utils.db_handling.db_access import DbRequirementsAccessor

def get_db_type(db_type, version):
    return db_type if not db_type == 'eggnog' else f"{db_type}_v{version[0]}"


rule download_using_direct_dl:
    output:
        protected("{database}/{version}/direct_dl/{filename}")
    message:
        f"downloading file: {'{output[0]}'}"
    version: "0.1"
    params:
        url=lambda w: DbRequirementsAccessor(get_db_type(w.database, w.version), w.version).get_url_for_file(w.filename)
    shell:
        """
        wget -nv -O "{output}" "{params.url}"
        """

rule download_using_emapper_v1_dl:
    output:
        # protecting emapper data results in snakemake crash, probably due to large number of files
        directory("eggnog/{version, 4.*}/emapper_dl/{directory}")
    message:
        f"downloading file using emapper: {'{output[0]}'}"
    conda:
        "../../envs/emapper_v1.yaml"
    shell:
        """
        mkdir -p "{output[0]}";
        download_eggnog_data.py -y -f --data_dir "{output[0]}" euk bact arch viruses;
        """


rule download_using_emapper_v2_dl:
    output:
        # protecting emapper data results in snakemake crash, probably due to large number of files
        directory("eggnog/{version, 5.*}/emapper_dl/{directory}")
    message:
        f"downloading file using emapper: {'{output[0]}'}"
    conda:
        "../../envs/emapper_v2.yaml"
    shell:
        """
        mkdir -p "{output[0]}";
        download_eggnog_data.py -y -f --data_dir "{output[0]}";
        """

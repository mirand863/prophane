import os

rule setup_skip_files:
    output:
        expand("skip/{f}", f=["skip.map", "skip.fa"])
    shell:
        """
        mkdir -p $(dirname {output[0]})
        touch {output}
        """

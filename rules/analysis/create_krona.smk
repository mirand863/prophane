import os


rule create_krona_xml:
  input:
    'tasks/{task}.quant'
  output:
    'tasks/{task}.xml'
  message:
    "creating plot xml ..."
  version:
    "0.2"
  conda:
    "../../envs/krona.yaml"
  params:
    script_path=os.path.join(workflow.basedir, "scripts", "format4krona.py")
  shell:
    "{params.script_path} {input[0]}"


rule create_krona_html:
  input:
    'tasks/{task}.xml'
  output:
    "plots/plot_of_{task}.html"
  message:
    "creating plot html ..."
  version:
    "0.2"
  conda:
    "../../envs/krona.yaml"
  shell:
    "ktImportXML -o {output[0]} {input[0]}"

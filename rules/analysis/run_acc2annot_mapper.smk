from utils.protein_group_sql_db import ProteinGroupSqlDbInteractor
from utils.progs.acc2annot_mapper import run_acc2annot_mapper


rule run_acc2annot_mapper:
    input:
        pg_db='pgs/protein_groups_db.sql',
        map=lambda w: config['tasks'][int(w.taskid)]['params']['path']
    output:
        'tasks/{annot_type}_annot_by_acc2annot_mapper_on_custom_map.v{db_version}.task{taskid}.map'
    message:
        "performing task {wildcards.taskid} using user-defined accession-to-annotation map ..."
    benchmark:
        "tasks/{annot_type}_annot_by_acc2annot_mapper_on_custom_map.v{db_version}.task{taskid}.map.benchmark.txt"
    resources:
        mem_mb=1024 * 2
    version:
        "0.1"
    run:
        accs = ProteinGroupSqlDbInteractor(input.pg_db).get_all_accs_from_pg_db()
        run_acc2annot_mapper(query_accessions=accs, custom_acc2tax_map=input.map, result_path=output[0])

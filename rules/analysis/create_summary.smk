from utils.helper_funcs_for_snakemake_workflow import get_task_lca, get_task_map, get_task_lca_support
from utils.create_summary import create_summary
from glob import glob


def input_create_summary(wildcards):
    files_dict = {"pg_db": 'pgs/protein_groups_db.sql'}
    db_base_dir = get_db_base_dir()
    files_dict["lca_files"] = []
    files_dict["lca_support_files"] = []
    files_dict["map_files"] = []
    for i, task_dict in enumerate(config['tasks']):
        files_dict["lca_files"].append(get_task_lca(i,task_dict,db_base_dir))
        files_dict["lca_support_files"].append(get_task_lca_support(i,task_dict,db_base_dir))
        files_dict["map_files"].append(get_task_map(i,task_dict,db_base_dir))
    files_dict["quant"] = 'tasks/quant.tsv'
    files_dict["mafft_files"] = rules.report_mafft.output
    return files_dict


rule create_summary:
    input:
        unpack(input_create_summary)
    output:
        'summary.txt'
    message:
        "creating summary ..."
    params:
        tasks=config['tasks'],
        sample_groups=config['sample_groups']
    run:
        mafft_files = glob(os.path.join(input["mafft_files"][0], 'mafft.*.txt'))
        create_summary(input["pg_db"], input["lca_files"], input["lca_support_files"], input["map_files"],
            input["quant"], mafft_files, str(output), params.tasks, params.sample_groups)

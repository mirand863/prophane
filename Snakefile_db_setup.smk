from utils.helper_funcs_for_snakemake_workflow import convert_dbs_names_to_yaml_paths
from utils.input_output import get_yamls_in_path


def get_all_input():
    # existing yamls
    yamls = get_yamls_in_path()
    # to be created based on config values
    missing_yamls = convert_dbs_names_to_yaml_paths(config["dbs"])
    yamls.extend(missing_yamls)
    return yamls


rule all:
    input:
        yml_files=get_all_input,
        skip_files=expand("skip/{f}", f=["skip.map", "skip.fa"])

include: "rules/db_setup/create_skip_files.smk"
include: "rules/db_setup/download_database_file.smk"
include: "rules/db_setup/check_md5.smk"
include: "rules/db_setup/create_db_cfg_yaml.smk"

# preprocessing
include: "rules/db_setup/gunzip.smk"
include: "rules/db_setup/make_dmnd_db.smk"
include: "rules/db_setup/make_hmm_lib.smk"

## map files
include: "rules/db_setup/make_dbcan_map.smk"
include: "rules/db_setup/make_eggnog_map.smk"
include: "rules/db_setup/make_foam_map.smk"
include: "rules/db_setup/make_nr_tax2annot_map.smk"
include: "rules/db_setup/make_nr_acc2tax_map.smk"
include: "rules/db_setup/make_pfam_map.smk"
include: "rules/db_setup/make_resfam_map.smk"
include: "rules/db_setup/make_tigrfam_map.smk"
include: "rules/db_setup/make_uniprot_acc2tax_map.smk"
include: "rules/db_setup/make_uniprot_tax2annot_map.smk"

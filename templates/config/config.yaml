# exemplary config file to define a PROPHANE job
# for more details see https://gitlab.com/s.fuchs/prophane/blob/master/templates/config/README.md

# define general job information here
general:
  job_name: "demonstration job"
  job_comment: "this job is for demonstration use"

# define input files here
input:
  ## GENERIC table - define generic table here; an example table can be found in the templates/input folder
  ## to check all available styles run: prophane --list-styles
  search_result: /path/to/test_job/input/generic_table.txt
  report_style: /path/to/prophane/styles/generic.yaml

  ## SCAFFOLD data - define Scaffold protein report file and file format style here
  ## to check all available styles run: prophane --list-styles
  # search_result: /path/to/jobdir/input/Protein_Report.xls
  # report_style: /path/to/prophane3/styles/scaffold_4_8_x.yaml

  ## METAPRPOTEOME ANALYZER data - define MPA's report files and file format style here
  ## to check all available styles run: prophane --list-styles
  ## please consider that a 'sample' column has to be added manually to the report
  # search_result: /path/to/jobdir/input/metaproteins.txt
  # report_style: /path/to/prophane3/styles/mpa_1_8_x.yaml

  ## define a list of used target databases (FASTA) here, format can be plain or gzipped
  fastas:
    - /path/to/jobdir/targetdb.fasta[.gz]
    
  # list of taxonomy maps (TSV) (if no map is used, use [])
  taxmaps: []

# define the desired output directory here
output_dir: /path/to/jobdir/

# define the desired analysis tasks here
# to check all available db_types run: prophane --list-dbs
tasks:
  ## exemplary task for taxonomic annotation based on Uniprot Trembl
  ## db_type: uniprot_tr, uniprot_sp, uniprot_complete, ncbi_nr
  ## mandatory param evalue: 0.01
  ## optional parameters (with default value): algo: 0, band: , block-size: 2.0, comp-based-stats: 1, dbsize: 40000000,
  ## frameshift: 0, freq-sd: , gapextend: 0, gapopen: 0, gapped-xdrop: , hit-band: , hit-score: , id: 0.0, id2: ,
  ## index-mode: 0, masking: 1, max-hsps: 1, rank-ratio: '', rank-ratio2: '', sensitive: '', shape-mask: ,
  ## matrix: 'BLOSUM62', strand: both, unapped-score: , window: , shapes: 0, xdrop: , query-cover: 0, more-sensitive: '', min-score: 20
  - prog: 'diamond blastp'
    type: taxonomic
    shortname: tax_from_trembl_20180808_qcover90
    db_type: uniprot_tr
    # db_version parameter is optional.
    # Valid values are the directory names of the version subdirectories of each DB. E.g. '31' for pfams DB (mind the ')
    # If db_version is missing or 'newest', the most recent downloaded DB is selected (if none is on disk, the
    # newest one is downloaded).
    db_version: 'newest'
    params:
      evalue: 0.01
      query-cover: 0.9

  ## exemplary task for functional annotation based on Eggnog
  ## mandatory param m: diamond OR hmmer
  ## optional parameters (with default value): gapextend: 0, gapopen: 0, go_evidence: 'experimental', gguessdb: 131567,
  ## hmm_maxhits:1, hmm_maxseqlen: 5000, hmm_qcov: 0, hmm_score: 20.0, query-cover: 0, seed_ortholog_score: 60.0,
  ## subject-cover: 0, target_orthologs: one2one, tax_scope: 131567, Z: 40000000
  - prog: emapper
    type: functional
    shortname: fun_from_eggNog_4.5.1
    db_type: eggnog
    params:
      m: diamond

  ## exemplary task for functional annotation based on Tigrfams
  ## db_types: pfams, tigrfams
  ## prog: hmmscan, hmmsearch
  ## optional parameters (with default value): E: number, T: number, cut_ga: '', cut_nc: '', cut_tc: '', domE: 10, domT:0.0, domZ: , F1: 0.02,
  ## F2: 0.001, F3: 0.00001, incdomE: 10, incomT: 0.0, incE: 10, incT: 0.0, nobias: '', nonull2: '', seed: 42, T: 0.0, Z: int
  - prog: hmmscan
    type: functional
    shortname: fun_from_TIGRFAMs_15_cut_tc
    db_type: tigrfams
    params:
      cut_tc: ''

  ## exemplary task for functional annotation based on FOAMs
  ## db_types: foam, dbcan, resfams_full, resfams_core
  ## prog: hmmscan, hmmsearch
  ## optional parameters (with default value): domE: 10, domT:0.0, domZ: , F1: 0.02, F2: 0.001, F3: 0.00001, incdomE: 10,
  ## incomT: 0.0, incE: 10, incT: 0.0, nobias: '', nonull2: '', seed: 42, T: 0.0, Z:
  - prog: hmmscan
    type: functional
    shortname: fun_from_FOAMs
    db_type: foam
    params:
      domE: 10

  ## exemplary task for retrieving annotation from a custom map file
  # the map file must be a  tab-separated table (tsv) file, with the first column holding accessions of the same format
  # as those in the proteomic search result, all additional columns will be used for annotation
  # Example tsv content: (remove leading '#')
#acc	level1	level2	level3
#KZK33282.1	Bacteria	Firmicutes	Bacilli
#NP_268346.1	Bacteria	Firmicutes	Bacilli
#WP_003131952.1	Bacteria	Firmicutes	Bacilli
#PKC80086.1	Bacteria	Actinobacteria	Bifidobacteriales
#XP_963622.1	Eukaryota	Ascomycota	Sordariomycetes
#WP_000665196.1	Bacteria	Proteobacteria	Gammaproteobacteria
#WP_000665196.1	Bacteria	Proteobacteria	Gammaproteobacteria group
  - prog: acc2annot_mapper
    type: taxonomic
    shortname: fun_from_my_file
    db_type: custom_map
    params:
      path: /path/to/accession2annotation_map.tsv

# define sample groups and sample names here
sample_groups:
    control:
      - "controls::R1"
      - "controls::R2"
      - "controls::R3"
    treated:
      - "treated::R1"
      - "treated::R2"
      - "treated::R3"

# define decoy accession using regular expression here
decoy_regex: DECOY$

# define quantification method here {max_nsaf, min_nsaf, mean_nsaf, raw}
quant_method: max_nsaf

# define lowest common ancestor method for taxonomic tasks and functional tasks
# 3 Options: no_lca, lca, democratic_lca,
# threshold for lca: minimal relative support [0, 1] (default: 0.5001)
lca:
  method: lca
  threshold: 0.5001

from utils.databases.superclasses.hmm_db_superclass import HmmDatabase


class PfamsDatabase(HmmDatabase):
    name = "pfams"
    valid_types = [name]
    mandatory_keys = HmmDatabase.mandatory_keys[:]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get_clan_txt_file(self, *args, **kwargs):
        f = self._config["files"]['clan_txt']
        path = self.join_with_db_path(f, *args, **kwargs)
        return path

    def get_pfam_clans_file(self, *args, **kwargs):
        f = self._config["files"]['pfam_clans']
        path = self.join_with_db_path(f, *args, **kwargs)
        return path

from utils.databases.superclasses.database_superclasses import DatabaseRequiringHelperDbs


class FastaDatabase(DatabaseRequiringHelperDbs):
    mandatory_keys = DatabaseRequiringHelperDbs.mandatory_keys[:]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get_fasta_gz_file_list(self, *args, **kwargs):
        f = self._config["files"]['fasta_gz']
        full_path = self.join_with_db_path(f, *args, **kwargs)
        return [full_path]

    @property
    def required_db_types(self):
        raise NotImplementedError

    def get_list_of_acc2tax_files(self):
        raise NotImplementedError

from utils.databases.superclasses.hmm_db_superclass import HmmDatabase


class ResfamsDatabase(HmmDatabase):
    mandatory_keys = HmmDatabase.mandatory_keys[:]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get_map_resource_file(self, *args, **kwargs):
        f = self._config["files"]['map_res_xlsx']
        path = self.join_with_db_path(f, *args, **kwargs)
        return path


class ResfamsCoreDatabase(ResfamsDatabase):
    name = 'resfams_core'
    valid_types = [name]


class ResfamsFullDatabase(ResfamsDatabase):
    name = 'resfams_full'
    valid_types = [name]

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd

from utils.search_result_parsing.helper_classes import ProteomicSearchResultReaderBaseClass, InputFile


class GenericTableReader(ProteomicSearchResultReaderBaseClass):

    def read_file_into_dataframe(self):
        file_obj = InputFile(self.result_table)
        required_col_names = self.style_dict['sample_cols'] \
                             + [self.style_dict['proteins_col'], self.style_dict['quant_col']]
        raw_df = pd.read_csv(self.result_table, sep=self.style_dict['field_sep'], encoding=file_obj.encoding)
        df = raw_df[required_col_names].copy()
        df['sample'] = df[self.style_dict['sample_cols']]\
            .apply(lambda x: '::'.join(x), axis=1)

        df['proteins'] = df[self.style_dict['proteins_col']].apply(lambda x: x.split(self.style_dict['proteins_sep']))

        df_standard = pd.DataFrame()
        df_standard['proteins'] = df['proteins']
        df_standard['sample'] = df['sample']
        df_standard['quant'] = df[self.style_dict['quant_col']]

        return df_standard

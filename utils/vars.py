import os

PROPHANE_VERSION = "6.2.6"
PROPHANE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
DEFAULT_GENERAL_CFG = os.path.join(os.getenv('HOME'), ".config", "prophane.yaml")
GENERAL_CFG_PATHS = [os.path.join(PROPHANE_DIR, 'general_config.yaml'),
                     DEFAULT_GENERAL_CFG,
                     os.path.join("/", "etc", "prophane.yaml")]
DB_RESSOURCE_DIR = os.path.join(PROPHANE_DIR, "utils", "databases")

REQUIRED_DB_SCHEMA_VERSION = 6
MAP_FILE_BASENAME = "id2annot"

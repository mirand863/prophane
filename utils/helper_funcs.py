import datetime

from pkg_resources import parse_version


def recursively_iter_dict(d):
    for k, v in d.items():
        if isinstance(v, dict):
            recursively_iter_dict(v)
        else:
            yield k, v


def get_timestamp():
    return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")


def get_max_version(list_of_version_numbers):
    v_max = ''
    for v in list_of_version_numbers:
        if parse_version(v) >= parse_version(v_max):
            v_max = v
    return v_max

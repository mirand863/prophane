#!/usr/bin/env bash

# set bash mode to strict
set -euo pipefail
IFS=$'\n\t'

# Test directory
TEST_DIR=${TMPDIR:-.}/prophane_test-${USER:-}
echo 'writing to '"${TEST_DIR:-.}"
mkdir -p "${TEST_DIR:-.}"

if [[ $(conda env list | grep -E "^prophane-test\s") ]]; then
    echo 'removing old environment "prophane-test"...'
    conda env remove -y -q --name prophane-test
fi

echo 'setting up testing environment...'
conda env create -q --name prophane-test --file test_environment.yaml
echo "Writing list of installed packages to '${TEST_DIR}/test_environment.yaml'"
conda env export --name prophane-test > "$TEST_DIR"/test_environment.yaml

echo "executing tests"
source "$(conda info --root)/etc/profile.d/conda.sh"
set +u
conda activate prophane-test
set -u
pytest -v --basetemp=$TEST_DIR/pytest tests
EXT_CD=$?
conda deactivate

exit "$EXT_CD"

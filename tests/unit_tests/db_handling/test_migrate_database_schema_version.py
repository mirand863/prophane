import pytest

import utils.db_handling.maintenance.migrate_database_schema_version as mig_gz
from utils.db_handling.maintenance.migrate_database_schema_version import assert_yaml_obj_has_target_version


def test_migrate_dbs_style_if_necessary(prophane_test_dbs_before_map_gz, tmpdir):
    mig_gz.migrate_dbs_style_if_necessary(prophane_test_dbs_before_map_gz)


def test_check_version_bump():
    from utils.databases.superclasses.database_superclasses import Database
    db = Database("tests/test_dbs/ncbi_nr/2018-08-08/db_config.yaml")
    version = db.get_db_schema_version()
    with pytest.raises(ValueError):
        assert_yaml_obj_has_target_version(db, version + 1)

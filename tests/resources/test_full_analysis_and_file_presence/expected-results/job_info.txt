PROPHANE JOB INFORMATION

a. general information
   job name: Test
   job comment: none
   start time: 2020-11-19 12:38:38
   end time: 2020-11-19 12:39:53

b. program versions
   prophane: 4.2.2
   git commit sha: 49c5ca5c8b8484c6c7dff5aeb3dc2707dead30a2

c. input
   sequence data:
      - target_db.fasta
   taxonomic data: None

d. sample groups
   sample
      - sample A::replicate 1

e. tasks
   task: 0
   task comment: tax_from_nr_20180808_qcover90_diamond
   algorithm: diamond blastp
   query: sequences without taxonomic classification (seqs/missing_taxa.faa)
   database: NCBI NR database
   database version: 2018-08-08
   database comment: test database containing the 100 first enries of the original database downloaded at 2018-08-08
   database scope: tax
   parameter:
      - evalue 0.01
      - query-cover 0.9
      - type  taxonomic
      - db_type  ncbi_nr

   task: 1
   task comment: tax_from_uniprot_20180808_qcover90_diamond
   algorithm: diamond blastp
   query: sequences without taxonomic classification (seqs/missing_taxa.faa)
   database: Uniprot Trembl and Swissprotdatabase
   database version: 2018-08-08
   database comment: both Trembl and Swissprot test databases, original databases were downloaded at 2018-08-08
   database scope: tax
   parameter:
      - evalue 0.01
      - query-cover 0.9
      - type  taxonomic
      - db_type  uniprot_complete

   task: 2
   task comment: tax_from_trembl_20180808_qcover90_diamond
   algorithm: diamond blastp
   query: sequences without taxonomic classification (seqs/missing_taxa.faa)
   database: Uniprot Trembl database
   database version: 2018-08-08
   database comment: test dump containing necessary data of the original data downloaded at 2018-08-08
   database scope: tax
   parameter:
      - evalue 0.01
      - query-cover 0.9
      - type  taxonomic
      - db_type  uniprot_tr

   task: 3
   task comment: tax_from_swissprot_20180808_qcover90_diamond
   algorithm: diamond blastp
   query: sequences without taxonomic classification (seqs/missing_taxa.faa)
   database: Uniprot Swissprot database
   database version: 2018-08-08
   database comment: test database containing the 100 first enries of the original database downloaded at 2018-08-08
   database scope: tax
   parameter:
      - evalue 0.01
      - query-cover 0.9
      - type  taxonomic
      - db_type  uniprot_sp

   task: 4
   task comment: fun_from_TIGRFAMs_15_cut_ga
   algorithm: hmmsearch
   query: all sequences (seqs/all.faa)
   database: Tigrfams
   database version: 15.0
   database comment: test database containing the 100 first entries of the original database downloaded at 2018-07-29
   database scope: func
   parameter:
      - cut_tc 
      - type  functional
      - db_type  tigrfams

   task: 5
   task comment: fun_from_PFAMs_31
   algorithm: hmmscan
   query: all sequences (seqs/all.faa)
   database: Pfam-A
   database version: 31
   database comment: test database containing the 100 first enries of the original database downloaded at 2018-07-29
   database scope: func
   parameter:
      - E 0.01
      - type  functional
      - db_type  pfams

   task: 6
   task comment: fun_from_FOAM_1a
   algorithm: hmmscan
   query: all sequences (seqs/all.faa)
   database: foam
   database version: rel1a
   database comment: test foam database
   database scope: func
   parameter:
      - E 0.01
      - type  functional
      - db_type  foam

   task: 7
   task comment: fun_from_RESFAMS_CORE_1.2
   algorithm: hmmscan
   query: all sequences (seqs/all.faa)
   database: Resfams (Core)
   database version: 1.2
   database comment: downloaded at 2019-11-26
   database scope: func
   parameter:
      - E 10
      - type  functional
      - db_type  resfams_core

   task: 8
   task comment: fun_from_RESFAMS_FULL_1.2
   algorithm: hmmscan
   query: all sequences (seqs/all.faa)
   database: Resfams (Full)
   database version: 1.2
   database comment: downloaded at 2019-11-26
   database scope: func
   parameter:
      - E 10
      - type  functional
      - db_type  resfams_full

   task: 9
   task comment: fun_from_DBCAN_v8
   algorithm: hmmscan
   query: all sequences (seqs/all.faa)
   database: dbCAN/CAzY
   database version: v8
   database comment: none
   database scope: func
   parameter:
      - E 10
      - type  functional
      - db_type  dbcan

f. lca
   lca method: democratic
   lca threshold: 1